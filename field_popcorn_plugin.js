
document.addEventListener("DOMContentLoaded", function() {

	var pop = Popcorn('#videojs-3-field-video-video', { pauseOnLinkClicked: true });
	pop.subtitle({
		start: 0,
		end: 2,
		target: 'story',
		text: 'Hi, my name is Sunny "the dog" Irwin and this is my story',
		image: 'img/sunny.jpg',
		href: 'http://tiptoes.ca'
	});
  pop.subtitle({
		start:2,
		target: 'story',
		text: 'My full name is Sunshine Fluffy Irwin (the kids named me) and I am part Westie & part Bichon.  I usually say \'Westiie X\' because Bichon is lap-dog, and that\'s just embarassing',
		end:6 
	})
	pop.image({
	  start:2, 
	  end: 6, 
	  src:'http://tiptoes.ca/drupal-popcorn/sites/default/modules/popcornfield/img/westie.jpg' , 
	  target:'story-image'
	})
	pop.wikipedia({
	  start:2, 
	  end: 6, 
	  target: 'story',
	  src: 'http://en.wikipedia.org/wiki/West_Highland_White_Terrier',
	  title: 'West Highland Terrier'
	});

        pop.subtitle({
		start: 6,
		target: 'tags',
		end: 10,
		text: 'I was named after Sunny Baudelaire from the Lemony Snicket series', 
	});
	pop.image({
		start:6, 
		end:10, 
		target: 'story-image', 
		href: 'http://en.wikipedia.org/wiki/Sunny_Baudelaire',
		src:  'http://tiptoes.ca/drupal-popcorn/sites/default/modules/popcornfield/img/Sunnyb.jpg'
	});
	pop.wikipedia({
		start:6,
		end:10,
		target: 'story',
		src: 'http://en.wikipedia.org/wiki/Sunny_Baudelaire',
		title: 'Wikipedia - Sunny Beadelaire',
		numberofwords: 100
	});
        pop.subtitle({
		start:10, 
		end:15, 
		target: 'story',
		text: 'I live in Sooke BC on Vancouver Island in Canada'
	}) 
	
	pop.googlemap({
	    start: 10, // Our start time in seconds
	    end: 15, // Our end time in seconds
	    type: "ROADMAP", //
            target: "story", // The id of our target DoM element
	    lat: 48.3742577, 
	    lng: -123.734461, 
	   zoom: 8 // Setting the zoom that we would like
	   });
	pop.subtitle({
	  start:15, 
	  end:20, 
	  person: 'It\'s REALLLLLY beautiful here - I like to go for walks on Whiffen Spit', 
	  target: 'story',

	});
	pop.image({
	 start:15, 
	 end:20, 
	 src: 'http://tiptoes.ca/drupal-popcorn/sites/default/modules/popcornfield/img/whiffen.jpg',
	 target: 'story'
	})

	pop.subtitle({
	 start:20, 
	 end:27, 
	 text: 'My name was inspiration for Emma\'s twitter account handle, although in hindsite @sunnydeveloper doesn\'t really make sense',
	 target: 'story'

	})

	pop.twitter({
	 start:20, 
	 end: 27, 
	 src: '@sunnydeveloper',
	 title:'Sunnydeveloper', 
	 target: 'story'
	})

	pop.subtitle({
	 start:27, 
	 end:35,
	 text: 'This is a proof-of-concept module.  Simply, a package of Drupal field types, you can add to a Drupal content type to interact with a video uploaded by the video.js module.  I\'m sure this will evolve, and if you are interested in helping please email:  emma.irwin@gmail.com   (We\'ll include a Github link soon)',
	 target: 'story',
	})

	pop.image({
	 start:27, 
	 end:35, 
	 src: 'http://tiptoes.ca/drupal-popcorn/sites/default/modules/popcornfield/img/drupalguy.png',
	 target: 'story'
})

	pop.image({
	 start:27, 
	 end:35, 
	 src: 'http://tiptoes.ca/drupal-popcorn/sites/default/modules/popcornfield/img/popcornjs3.png', 
	 target: 'story'
	})


	pop.webpage({
		start: 16,
		target: 'side',
		src: 'http://mozillapopcorn.org/'
	});

	pop.play();

}, false);
